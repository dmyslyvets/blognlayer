﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogNLayer.Entities
{
    [Table("CategoryInArticles")]
    public class CategoryInArticle : BaseEntity
    {
        public Guid ArticleId { get; set; }
        public Guid CategoryId { get; set; }
    
    }
}
