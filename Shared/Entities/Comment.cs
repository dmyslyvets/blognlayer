﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogNLayer.Entities
{
    public class Comment : BaseEntity
    {
        public string NameAuthor { get; set; }
        public string Commentary { get; set; }
        public DateTime DateTime { get; set; }
    
        public string ArticleId { get; set; }
        //public Article Article { get; set; }
    }
}
