﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogNLayer.ViewModels.CommentViewModels
{
     public class CommentViewModel
    {
        public Guid Id { get; set; }
        public string NameAuthor { get; set; }
        public string Commentary { get; set; }
        public DateTime DateTime { get; set; }
        public string ArticleId { get; set; }
    }
}
