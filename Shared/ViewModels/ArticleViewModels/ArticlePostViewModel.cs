﻿using System;
using System.Collections.Generic;
using BlogNLayer.Entities;

namespace BlogNLayer.ViewModels.ArticleViewModels
{
    public class ArticlePostViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public List<string> CategoryId { get; set; }
    }
}
