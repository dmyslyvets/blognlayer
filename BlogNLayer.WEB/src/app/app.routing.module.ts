import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {AppComponent} from "./app.component";
import {APP_BASE_HREF} from '@angular/common';
import { AllArticleComponent } from './article/allArticles/allArticle.component';
import { NewArticleComponent } from './article/newArticle/newArticle.component';
import { AllCategoryComponent } from './category/allCategory/allCategory.component';
import { EditCategoryComponent } from './category/editCategories/editCategory.component';
import { ArticleComponent } from './article/separateArticle/article.component';
 
import {   } from './lazy/article.module';

const routes: Routes = [
    { path: "", loadChildren: './lazy/allArticle.module#AllArticleModule' },
    { path: "allArticle", loadChildren: './lazy/allArticle.module#AllArticleModule' },
    { path: "newArticle", loadChildren: './lazy/newArticle.module#ArticleModule'},
    { path: "allCategory", loadChildren: './lazy/allCategory.module#AllCategoryModule' },
    { path: "editCategory", loadChildren: './lazy/editCategories.module#EditCategoryModule' },
    { path: 'article', loadChildren: './lazy/article.module#ArticleModule' } 
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [{provide: APP_BASE_HREF, useValue : '/' }]
})
export class AppRoutingModule{}