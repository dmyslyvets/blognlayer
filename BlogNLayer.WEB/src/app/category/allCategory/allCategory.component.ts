import { Component, style } from '@angular/core';
import { ArticleService } from '../../shared/service/article.service';
import { CategoryService } from '../../shared/service/category.service';
import { ArticleModel } from '../../shared/models/articleModel';
import { Category } from '../../shared/models/category';
import { Router } from '@angular/router';


@Component({
    moduleId: module.id,
    selector: 'allCategory',
    templateUrl: "allCategory.component.html"
})

export class AllCategoryComponent {
    Category: Category[] = new Array();
    selectedValue: string;
    ArticleViewModel: ArticleModel[] = [];

    constructor(private categoryService: CategoryService, private articleService: ArticleService, private router: Router) { }

    ngOnInit() {
        this.categoryService.getAll().subscribe((data: Category[]) => { this.Category = data; });
    }
    selectArticle(id: string) {
        this.router.navigate(["article", id]);
    }
 
    valueChange() {
        if (this.selectedValue == null) {
            return;
        }
        this.articleService.getAllByCategory(this.selectedValue).subscribe((data: ArticleModel[]) => { this.ArticleViewModel = data;});
    }
}
