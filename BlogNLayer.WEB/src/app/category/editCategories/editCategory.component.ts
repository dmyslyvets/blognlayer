import { Component, OnInit, Inject } from '@angular/core';
import { Category } from '../../shared/models/category';
import { CategoryService } from '../../shared/service/category.service';
import { Observable } from 'rxjs/Rx';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';

@Component({

    moduleId: module.id,
    selector: 'editCategory',
    templateUrl: "editCategory.component.html"
})

export class EditCategoryComponent {
    public categories: Category[] = new Array();
    public gridState: State = {
        sort: [],
        skip: 0,
        take: 10
    };
    public formGroup: FormGroup;
    private editedRowIndex: number;

    constructor(private categoryService: CategoryService) { }

    ngOnInit() {
        this.categoryService.getAll().subscribe((data: Category[]) => { this.categories = data;});
    }

    public addHandler({ sender }) {
        this.closeEditor(sender);

        this.formGroup = new FormGroup({
            'Title': new FormControl(),
        });
        sender.addRow(this.formGroup);
    }

    public saveHandler({ sender, rowIndex, formGroup, isNew }) {
        const category: Category = formGroup.value;
        this.categoryService.add(category).subscribe((data: Category[]) => { this.categories = data; });
        sender.closeRow(rowIndex);
    }

    public removeHandler({ dataItem }) {
        this.categoryService.delete(dataItem.id);
        let index = this.categories.indexOf(dataItem);
        this.categories.splice(index, 1);
    }

    public cancelHandler({ sender, rowIndex }) {
        this.closeEditor(sender, rowIndex);
    }

    private closeEditor(grid, rowIndex = this.editedRowIndex) {
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    }
}
