import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ArticleSend } from '../models/articleSend';
import { Category } from '../models/Category';
import { ConstHelperService } from './constHelper.service';

@Injectable()
export class ArticleService {

    constructor(private constHelper: ConstHelperService, private http: HttpClient) {

    }

    getAll() {
        return this.http.get(this.constHelper.getHost() + 'api/Article/GetAllArticle', { responseType: "json" });
    }

    get(id: string) {
        return this.http.get(this.constHelper.getHost() + 'api/Article/GetArticle/' + id, { responseType: "json" });
    }

    getAllByCategory(id: string) {
        return this.http.get(this.constHelper.getHost() + 'api/Article/GetAllArticleByCategory/' + id, { responseType: "json" });
    }

    delete(id: string) {
        const body = id;
        this.http.post(this.constHelper.getHost() + 'api/Article/Delete/' + id, body).subscribe();
    }

    post(article: ArticleSend) {
        const body = { Title: article.title, Author: article.author, Description: article.description, CategoryId: article.categoryId, };
        this.http.post(this.constHelper.getHost() + 'api/Article/CreateArticle', body).subscribe();
    }
} 