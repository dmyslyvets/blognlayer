export class ArticleSend {
    constructor(
        public title: string,
        public author: string,
        public description: string,
        public categoryId: string[]
    ) { }
} 