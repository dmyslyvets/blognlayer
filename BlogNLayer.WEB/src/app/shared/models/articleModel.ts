import { Category } from '../models/category';
import { Comment } from '../models/comment';

export class ArticleModel {
    public id: string;
    public title: string;
    public description: string;
    public author: string;
    public comment: Comment[];
    public category: Category[];
    public dateTime: string;
}