import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app.routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';

//My components
import { CommentService } from './shared/service/comment.service';
import { ArticleService } from './shared/service/article.service';
import { CategoryService } from './shared/service/category.service';
import { ConstHelperService } from './shared/service/constHelper.service';

//import { AllArticleComponent } from './article/allArticles/allArticle.component';
//import { NewArticleComponent } from './article/newArticle/newArticle.component';
///import { AllCategoryComponent } from './category/allCategory/allCategory.component';
//import { EditCategoryComponent } from './category/editCategories/editCategory.component';
//import { ArticleComponent } from './article/separateArticle/article.component';

// Import the Animations module
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Import the ButtonsModule
//import { ButtonsModule } from '@progress/kendo-angular-buttons';
//import { GridModule } from '@progress/kendo-angular-grid';
//import { DropDownsModule } from '@progress/kendo-angular-dropdowns';

  

@NgModule({
  declarations: [
    AppComponent,
    //AllArticleComponent,
  //  NewArticleComponent,
    //AllCategoryComponent,
    //EditCategoryComponent,
    //ArticleComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    //GridModule,
    AppRoutingModule,
    //DropDownsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    //ButtonsModule
  ],

  providers: [
    CommentService,
    ArticleService,
    CategoryService,
    ConstHelperService
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
