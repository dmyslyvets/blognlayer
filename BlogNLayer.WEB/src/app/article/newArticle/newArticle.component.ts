import { Component } from '@angular/core';
 
import { ArticleSend } from '../../shared/models/articleSend';
import { ArticleService } from '../../shared/service/article.service';
import { CategoryService } from '../../shared/service/category.service';
import { Category } from '../../shared/models/category';


@Component({

    moduleId: module.id,
    selector: 'newArticle',
    templateUrl: "newArticle.component.html",
    styles: ['.countries { width: 300px; }']
})

export class NewArticleComponent {
    Title: string;
    Name: string;
    Discription: string;
    CategoryId: string[] = new Array();
    Category: Category[] = new Array();
    selectedValue: string[] = new Array();

    constructor(private articleService: ArticleService, private categoryService: CategoryService) { }

    ngOnInit() {
        this.categoryService.getAll().subscribe((data: Category[]) => { this.Category = data;});
    }

    create() {
        this.CategoryId = this.selectedValue;
       
        let article: ArticleSend = new ArticleSend(this.Title, this.Name, this.Discription, this.CategoryId);
        console.log(article);
        this.articleService.post(article);
        this.selectedValue = null;
        this.Title = null;
        this.Name = null;
        this.Discription = null;
    }
}
