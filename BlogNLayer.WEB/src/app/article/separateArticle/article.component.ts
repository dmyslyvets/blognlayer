import { Component } from '@angular/core';
import { ArticleService } from '../../shared/service/article.service';
import { CommentService } from '../../shared/service/comment.service';
import { ArticleModel } from '../../shared/models/articleModel';
import { Comment } from '../../shared/models/comment';
import { CommentSend } from '../../shared/models/commentSent';
import { ActivatedRoute } from '@angular/router';


@Component({

    moduleId: module.id,
    selector: 'article',
    templateUrl: "article.component.html"
})
export class ArticleComponent {

    articleModel: ArticleModel = new ArticleModel();
    test: number;
    name: string;
    commentary: string;

    comments: Comment[] = new Array();
    id: string;
    constructor(private activateRoute: ActivatedRoute, private articleService: ArticleService, private commentService: CommentService) {
        this.id = activateRoute.snapshot.params['id'];
    }

    ngOnInit() {
        this.articleService.get(this.id).subscribe((data: ArticleModel) => { this.articleModel = data; this.comments = this.articleModel.comment });
    }

    addComment() {
        let coment: CommentSend = new CommentSend(this.name, this.commentary, this.articleModel.id);
        console.log(coment);
        this.commentService.addComment(coment);
        this.name = null;
        this.commentary = null;
        this.articleService.get(this.id).subscribe((data: ArticleModel) => { this.articleModel = data; this.comments = this.articleModel.comment });
    }

    delete(comment: Comment) {
        this.commentService.deleteComent(comment.id);
        let index = this.articleModel.comment.indexOf(comment);
        this.articleModel.comment.splice(index, 1);
    }

}
