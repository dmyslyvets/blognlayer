import { Component, Input } from '@angular/core';
import { ArticleService } from '../../shared/service/article.service';
import { Router } from '@angular/router';
import { ArticleModel } from '../../shared/models/articleModel';
import { Comment } from '@angular/compiler';

@Component({
    moduleId: module.id,
    selector: 'allArticledT',
    templateUrl: "allArticle.component.html"
})
export class AllArticleComponent {

    articleModel: ArticleModel[];

    constructor(private articleService: ArticleService, private router: Router) { }

    ngOnInit() {
        this.articleService.getAll().subscribe((data: ArticleModel[]) => { this.articleModel = data; console.log(this.articleModel)});
    }

    onSelect(id: string) {
        this.router.navigate(["article", id]);
    }

    dell(article: ArticleModel) {
       
        this.articleService.delete(article.id);
        let index = this.articleModel.indexOf(article);
        this.articleModel.splice(index, 1);
    }
}
