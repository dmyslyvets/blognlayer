
//My components
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { ArticleComponent } from '../article/separateArticle/article.component';
 
// Import the Animations module
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

const routes: Routes = [
    { path: ":id", component: ArticleComponent }
  ];

 

@NgModule({
  declarations: [
    ArticleComponent
  ],

  imports: [ 
    RouterModule.forChild(routes),
    CommonModule,
   FormsModule
  ]
})
export class ArticleModule { }
