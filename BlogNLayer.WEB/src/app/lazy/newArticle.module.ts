
//My components
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { NewArticleComponent } from '../article/newArticle/newArticle.component';
 
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';


// Import the Animations module
 
const routes: Routes = [
  { path: "newArticle", component: NewArticleComponent }
];

@NgModule({
  declarations: [
    NewArticleComponent,
  ],

  imports: [
    RouterModule.forChild(routes),
    DropDownsModule,
   //  BrowserAnimationsModule,
    CommonModule,
    FormsModule
  ]
})
export class ArticleModule { }
