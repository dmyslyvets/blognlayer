import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AllArticleComponent } from '../article/allArticles/allArticle.component';
 

import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';


const routes: Routes = [
    { path: "allArticle", component: AllArticleComponent }
  ];

 

@NgModule({
  declarations: [ 
    AllArticleComponent
  ],

  imports: [
    RouterModule.forChild(routes),
    CommonModule,
 
    FormsModule
  ]  
})
export class AllArticleModule { }
 