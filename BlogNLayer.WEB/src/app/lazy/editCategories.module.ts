

import { EditCategoryComponent  } from '../category/editCategories/editCategory.component';

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {CommonModule} from '@angular/common';


import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { GridModule } from '@progress/kendo-angular-grid';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: "editCategory", component: EditCategoryComponent }
  ];

   

@NgModule({   
  declarations: [
    EditCategoryComponent
  ],

  imports: [
    RouterModule.forChild(routes),
    DropDownsModule,
    
    GridModule,
    FormsModule
    
    
  ]

 
})
export class EditCategoryModule { }