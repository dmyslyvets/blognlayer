
 
import { AllCategoryComponent } from '../category/allCategory/allCategory.component';

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {CommonModule} from '@angular/common';

//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { GridModule } from '@progress/kendo-angular-grid';

import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: "allCategory", component: AllCategoryComponent }
  ];

 

@NgModule({   
  declarations: [
    AllCategoryComponent
  ],

  imports: [
    RouterModule.forChild(routes),
    DropDownsModule,
   // BrowserAnimationsModule,
    GridModule,
    FormsModule,
    CommonModule
  ]

 
})
export class AllCategoryModule { }
