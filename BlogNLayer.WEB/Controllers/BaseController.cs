using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace BlogNLayer.WEB.Core.Controllers
{
  [Produces("application/json")]
  [Route("api/Base")]

  public class BaseController : Controller
  {
    protected  string connectionString { get; set; }

    public  BaseController(IConfiguration configuration)
    {
      connectionString = configuration.GetConnectionString("DefaultConnection");
    }

  }
}
