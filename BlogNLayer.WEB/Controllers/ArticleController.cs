using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BlogNLayer.Service;
using BlogNLayer.ViewModels;
using BlogNLayer.ViewModels.ArticleViewModels;
using BlogNLayer.WEB.Core.Controllers;
using Microsoft.Extensions.Configuration;

namespace BlogNLayerCore.Controllers
{

    [Route("api/[controller]/[action]")]
    public class ArticleController : BaseController 
    {
        private ArticleService _articleService;
    
        public ArticleController(IConfiguration configuration) : base(configuration)
        {
           
            _articleService = new ArticleService(connectionString);
        }

        [HttpGet("{id}")]
        public ArticleViewModel GetArticle(string id)
        {
            ArticleViewModel article = _articleService.GetArticle(id);
            if (article == null)
            {
                return null;
            }
            return article;
        }

        public List<ArticleViewModel> GetAllArticle()
        {
            return _articleService.GetAll();
        }

        [HttpGet("{id}")]
        public List<ArticleViewModel> GetAllArticleByCategory(string id)
        {
            List<ArticleViewModel> article = _articleService.GetAllArticleByCategory(id);
            if (article == null)
            {
                return null;
            }    
            return article;
        }

        [HttpPost]
        public void CreateArticle([FromBody]ArticlePostViewModel article)
        {
            _articleService.Create(article);
        }

        [HttpPost("{id}")]
        public void Delete(string id)
        {
            _articleService.Delete(id);
        }
    }
}
