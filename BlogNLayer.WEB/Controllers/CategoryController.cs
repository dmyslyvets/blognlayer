using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BlogNLayer.Service;
using BlogNLayer.ViewModels.CategoryViewModels;
using BlogNLayer.WEB.Core.Controllers;
using Microsoft.Extensions.Configuration;

namespace BlogNLayerCore.Controllers
{
    [Route("api/[controller]/[action]")]
    public class CategoryController : BaseController
  {
        private CategoryService _categoryService;

        public CategoryController(IConfiguration configuration) : base(configuration)
    {
           
           _categoryService = new CategoryService(connectionString);
        }

        [HttpGet]
        public List<CategoryViewModel> GetAll()
        {
            List<CategoryViewModel> categories = _categoryService.GetAllCategoryViews();

            return categories;
        }

        [HttpPost]
        public void Create([FromBody]CategoryPostViewModel category)
        {
            List<CategoryViewModel> categories = new List<CategoryViewModel>();
            if (category != null)
            {
                _categoryService.Create(category);
                categories = _categoryService.GetAllCategoryViews();
            }
           
        }

        [HttpPost("{id}")]
        public void  Delete(string id)
        {
            _categoryService.Delete(id);
        }
    }
}
