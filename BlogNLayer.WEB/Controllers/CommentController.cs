using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BlogNLayer.Service;
using BlogNLayer.ViewModels;
using BlogNLayer.ViewModels.CommentViewModels;
using BlogNLayer.WEB.Core.Controllers;
using Microsoft.Extensions.Configuration;

namespace BlogNLayerCore.Controllers
{
    [Route("api/[controller]/[action]")]
    public class CommentController : BaseController
  {
        private CommentService _commentService;

        public CommentController(IConfiguration configuration) : base(configuration)
    {
          
          _commentService = new CommentService(connectionString);
        }

        [HttpGet("{id}")]
        public List<CommentViewModel> GetComments(string id)
        {
            List<CommentViewModel> coments = _commentService.GetAllCommentsForArticle(id);
            return coments;
        } 

        [HttpPost]
        public void CreateComment([FromBody]CommentPostViewModel commentView)
        {
            if (commentView != null)
            {
                _commentService.Create(commentView);
            }
        }

        [HttpPost("{id}")]
        public void Delete(string id)
        {
            _commentService.Delete(id);
        }
    }
}
