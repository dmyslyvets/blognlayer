webpackJsonp(["editCategories.module"],{

/***/ "../../../../../src/app/category/editCategories/editCategory.component.html":
/***/ (function(module, exports) {

module.exports = "<h1>Edit Categories</h1>\r\n<div class=\"example-wrapper\">\r\n     \r\n    <kendo-grid\r\n    [data]=\"categories\"\r\n    [height]=\"533\"\r\n    [pageSize]=\"gridState.take\" [skip]=\"gridState.skip\" [sort]=\"gridState.sort\"\r\n    [pageable]=\"true\" [sortable]=\"true\"\r\n    (dataStateChange)=\"onStateChange($event)\"\r\n    (cancel)=\"cancelHandler($event)\"\r\n    (save)=\"saveHandler($event)\" (remove)=\"removeHandler($event)\"\r\n    (add)=\"addHandler($event)\"\r\n  >\r\n<ng-template kendoGridToolbarTemplate>\r\n    <button kendoGridAddCommand>Add new</button>\r\n</ng-template>\r\n<kendo-grid-column field=\"id\" title=\"Id\"></kendo-grid-column>\r\n<kendo-grid-column field=\"title\" title=\"Category\"></kendo-grid-column>\r\n<kendo-grid-command-column title=\"command\" width=\"220\">\r\n    <ng-template kendoGridCellTemplate let-isNew=\"isNew\">\r\n        <button kendoGridRemoveCommand>Remove</button>\r\n        <button kendoGridSaveCommand [disabled]=\"formGroup?.invalid\">{{ isNew ? 'Add' : 'Update' }}</button>\r\n        <button kendoGridCancelCommand>{{ isNew ? 'Discard changes' : 'Cancel' }}</button>\r\n    </ng-template>\r\n</kendo-grid-command-column>\r\n</kendo-grid>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/category/editCategories/editCategory.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditCategoryComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_service_category_service__ = __webpack_require__("../../../../../src/app/shared/service/category.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EditCategoryComponent = (function () {
    function EditCategoryComponent(categoryService) {
        this.categoryService = categoryService;
        this.categories = new Array();
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
    }
    EditCategoryComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.categoryService.getAll().subscribe(function (data) { _this.categories = data; });
    };
    EditCategoryComponent.prototype.addHandler = function (_a) {
        var sender = _a.sender;
        this.closeEditor(sender);
        this.formGroup = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormGroup */]({
            'Title': new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */](),
        });
        sender.addRow(this.formGroup);
    };
    EditCategoryComponent.prototype.saveHandler = function (_a) {
        var _this = this;
        var sender = _a.sender, rowIndex = _a.rowIndex, formGroup = _a.formGroup, isNew = _a.isNew;
        var category = formGroup.value;
        this.categoryService.add(category).subscribe(function (data) { _this.categories = data; });
        sender.closeRow(rowIndex);
    };
    EditCategoryComponent.prototype.removeHandler = function (_a) {
        var dataItem = _a.dataItem;
        this.categoryService.delete(dataItem.id);
        var index = this.categories.indexOf(dataItem);
        this.categories.splice(index, 1);
    };
    EditCategoryComponent.prototype.cancelHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex;
        this.closeEditor(sender, rowIndex);
    };
    EditCategoryComponent.prototype.closeEditor = function (grid, rowIndex) {
        if (rowIndex === void 0) { rowIndex = this.editedRowIndex; }
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    };
    EditCategoryComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            moduleId: module.i,
            selector: 'editCategory',
            template: __webpack_require__("../../../../../src/app/category/editCategories/editCategory.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__shared_service_category_service__["a" /* CategoryService */]])
    ], EditCategoryComponent);
    return EditCategoryComponent;
}());



/***/ }),

/***/ "../../../../../src/app/lazy/editCategories.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditCategoryModule", function() { return EditCategoryModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__category_editCategories_editCategory_component__ = __webpack_require__("../../../../../src/app/category/editCategories/editCategory.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__progress_kendo_angular_dropdowns__ = __webpack_require__("../../../../@progress/kendo-angular-dropdowns/dist/es/main.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__progress_kendo_angular_grid__ = __webpack_require__("../../../../@progress/kendo-angular-grid/dist/es/main.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    { path: "editCategory", component: __WEBPACK_IMPORTED_MODULE_0__category_editCategories_editCategory_component__["a" /* EditCategoryComponent */] }
];
var EditCategoryModule = (function () {
    function EditCategoryModule() {
    }
    EditCategoryModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_0__category_editCategories_editCategory_component__["a" /* EditCategoryComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_5__angular_router__["c" /* RouterModule */].forChild(routes),
                __WEBPACK_IMPORTED_MODULE_3__progress_kendo_angular_dropdowns__["c" /* DropDownsModule */],
                __WEBPACK_IMPORTED_MODULE_4__progress_kendo_angular_grid__["a" /* GridModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormsModule */]
            ]
        })
    ], EditCategoryModule);
    return EditCategoryModule;
}());



/***/ })

});
//# sourceMappingURL=editCategories.module.chunk.js.map