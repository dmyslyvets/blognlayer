﻿using System;
using System.Collections.Generic;
using System.Linq;
using BlogNLayer.Entities;
using BlogNLayer.Repositories;
using BlogNLayer.ViewModels.CommentViewModels;

namespace BlogNLayer.Service
{
    public class CommentService
    {
        private CommentRepositories _commentRepository;
        private Mapper _maper;

        public CommentService(string connectionString)
        {
            _maper = new Mapper();
            _commentRepository = new CommentRepositories(connectionString);
        }

        public List<CommentViewModel> GetAllCommentsForArticle(string idArticle)
        {
            List<CommentViewModel> commentsView = null;
            List<Comment> Comments = _commentRepository.GetAllForArticle(idArticle);
            commentsView = _maper.ComentToView.Map<List<Comment>, List<CommentViewModel>>(Comments);
            return commentsView;
        }

        public void Create(CommentPostViewModel commentView)
        {
            Comment comment = _maper.ViewToComment.Map<CommentPostViewModel, Comment>(commentView);
            comment.DateTime = DateTime.Now;
            _commentRepository.Create(comment);
        }

        public void Delete(string id)
        {
            _commentRepository.Delete(id);
        }
    }
}
