﻿using System;
using System.Collections.Generic;
using BlogNLayer.Entities;
using BlogNLayer.Repositories;
using BlogNLayer.ViewModels;
using BlogNLayer.ViewModels.ArticleViewModels;

namespace BlogNLayer.Service
{
    public class ArticleService
    {
        private ArticleRepositories _articleRepository;
        private CommentRepositories _comentRepository;
        private CategoryInArtycleRepositories _categoryInArticleRepository;
        private Mapper _maper;

        public ArticleService(string connectionString)
        {
            _maper = new Mapper();
            _articleRepository = new ArticleRepositories(connectionString);
            _comentRepository = new CommentRepositories(connectionString);
            _categoryInArticleRepository = new CategoryInArtycleRepositories(connectionString);
        }

        public List<ArticleViewModel> GetAllArticleByCategory(string categoryId)
        {
            List<ArticleViewModel> articlesView;
            var article = new List<Article>();
            article = _articleRepository.GetAllByCategory(categoryId);
            articlesView = _maper.ArticleToView.Map<List<Article>, List<ArticleViewModel>>(article);
            return articlesView;
        }

        public List<ArticleViewModel> GetAll()
        {
            var articles = new List<ArticleViewModel>();
            articles = _articleRepository.GetAll();
            return articles;
        }

        public ArticleViewModel GetArticle(string id)
        {
            ArticleViewModel ArticleViewModel = _articleRepository.Get(id);
            return ArticleViewModel;
        }

        public void Create(ArticlePostViewModel ArticleViewModel)
        {
            Article article = _maper.ViewToArticle.Map<ArticlePostViewModel, Article>(ArticleViewModel);

            if (article == null)
            {
                return;
            }

            article.DateTime = DateTime.Now;
            _articleRepository.Create(article);
            _categoryInArticleRepository.Create(article.Id, ArticleViewModel.CategoryId);
        }

        public void Delete(string id)
        {
            _comentRepository.DeleteForArticle(id);
            _categoryInArticleRepository.DeleteArticle(id);
            _articleRepository.Delete(id);
        }
    }
}
