﻿using System;
using System.Collections.Generic;
using BlogNLayer.Entities;
using BlogNLayer.Repositories;
using BlogNLayer.ViewModels.CategoryViewModels;

namespace BlogNLayer.Service
{
    public class CategoryService
    {
        private CategoryRepositories _categoryRepositories;
        private CategoryInArtycleRepositories _categoryInArtycleRepositories;
        private Mapper _maper;

        public CategoryService(string connectionString)
        {
            _maper = new Mapper();
            _categoryRepositories = new CategoryRepositories( connectionString);
            _categoryInArtycleRepositories = new CategoryInArtycleRepositories(connectionString);
        }

        public List<CategoryViewModel> GetAllCategoryViews()
        {
            List<CategoryViewModel> categories;
            categories = _maper.CategoryToView.Map<IEnumerable<Category>, List<CategoryViewModel>>(_categoryRepositories.GetAll());
            return categories;
        }

        public Category GetCategory(string id)
        {
            return _categoryRepositories.Get(id);
        }

        public void Create(CategoryPostViewModel CategoryViewModel)
        {
            Category category = _maper.ViewToCategory.Map<CategoryPostViewModel, Category>(CategoryViewModel);
            if (category != null)
            {
                _categoryRepositories.Create(category);
            }
        }

        public void Delete(string id)
        {
            _categoryInArtycleRepositories.DeleteCategory(id);
            _categoryRepositories.Delete(id);
        }
    }
}
