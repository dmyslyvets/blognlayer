﻿using AutoMapper;
using BlogNLayer.Entities;
using BlogNLayer.ViewModels;
using BlogNLayer.ViewModels.ArticleViewModels;
using BlogNLayer.ViewModels.CategoryViewModels;
using BlogNLayer.ViewModels.CommentViewModels;

namespace BlogNLayer.Service
{
    public class Mapper
    {
        private MapperConfiguration _comentToCommentView;
        private MapperConfiguration _comentViewToComment;
        private MapperConfiguration _articleToArticelView;
        private MapperConfiguration _articleViewToArticle;
        private MapperConfiguration _categoryToCategoryView;
        private MapperConfiguration _categoryViewToCategory;

        public IMapper ComentToView;
        public IMapper ViewToComment;
        public IMapper ArticleToView;
        public IMapper ViewToArticle;
        public IMapper CategoryToView;
        public IMapper ViewToCategory;

        public Mapper()
        {
             _comentToCommentView = new MapperConfiguration(cfg => cfg.CreateMap<Comment, CommentViewModel>());
             _comentViewToComment = new MapperConfiguration(cfg => cfg.CreateMap<CommentPostViewModel, Comment>());
             _articleToArticelView = new MapperConfiguration(cfg => cfg.CreateMap<Article, ArticleViewModel>());
             _articleViewToArticle = new MapperConfiguration(cfg => cfg.CreateMap<ArticlePostViewModel, Article>());
             _categoryToCategoryView = new MapperConfiguration(cfg => cfg.CreateMap<Category, CategoryViewModel>());
             _categoryViewToCategory = new MapperConfiguration(cfg => cfg.CreateMap<CategoryPostViewModel, Category>());

            ComentToView = _comentToCommentView.CreateMapper();
            ViewToComment = _comentViewToComment.CreateMapper();
            ArticleToView = _articleToArticelView.CreateMapper();
            ViewToArticle = _articleViewToArticle.CreateMapper();
            CategoryToView = _categoryToCategoryView.CreateMapper();
            ViewToCategory = _categoryViewToCategory.CreateMapper();
        }
    }
}
