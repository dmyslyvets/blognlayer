﻿using System.Collections.Generic;
using System.Linq;
using BlogNLayer.Entities;
using BlogNLayer.ViewModels;
using System;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using Dapper.Contrib.Extensions;
using BlogNLayer.ViewModels.ArticleViewModels;
using BlogNLayer.ViewModels.CommentViewModels;

namespace BlogNLayer.Repositories
{
    public class ArticleRepositories
    {
        private string _connectionString;

        public ArticleRepositories(string connectionString)
        {
            _connectionString = connectionString;
        }

        public List<ArticleViewModel> GetAll()
        {
            var articles = new List<ArticleViewModel>();
            var articleCategoryQuery = @"SELECT Articles.*, Categories.*, Comments.*
                                         FROM Articles
                                         INNER JOIN CategoryInArticles ON CategoryInArticles.ArticleId = Articles.Id
                                         INNER JOIN Categories ON Categories.Id = CategoryInArticles.CategoryId
                                         LEFT JOIN Comments  ON Comments.ArticleId = Articles.Id";

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var dictionaryArticle = new Dictionary<Guid, ArticleViewModel>();

                db.Query<ArticleViewModel, Category, CommentViewModel, ArticleViewModel>(articleCategoryQuery, (art, cat, com) =>
                {
                    ArticleViewModel ArticleViewModel = new ArticleViewModel();
                    if(!dictionaryArticle.TryGetValue(art.Id, out ArticleViewModel))
                    {
                        dictionaryArticle.Add(art.Id, ArticleViewModel = art);
                    }

                    if(ArticleViewModel.Category == null)
                    {
                        ArticleViewModel.Category = new List<Category>();
                    }

                    if(ArticleViewModel.Category.Find(x => x.Id == cat.Id) == null)
                    {
                        ArticleViewModel.Category.Add(cat);
                    }

                    if (ArticleViewModel.Comment == null)
                    {
                        ArticleViewModel.Comment = new List<CommentViewModel>();
                    }

                    if(com != null && ArticleViewModel.Comment.Find(x => x.Id == com.Id) == null)
                    { 
                        ArticleViewModel.Comment.Add(com);
                    }
                   
                    return ArticleViewModel;
                }).AsQueryable();

                 articles = dictionaryArticle.Values.ToList();
            }
            return articles;
        }

        public List<Article> GetAllByCategory(string id)
        {
            List<Article> articles;
            var query = @"SELECT Articles.*   
                          FROM Articles
                          LEFT JOIN CategoryInArticles ON CategoryInArticles.ArticleId = Articles.Id
                          WHERE CategoryInArticles.CategoryId = @Id";

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                articles = db.Query<Article>(query, new { Id = id }).ToList();
            }

            return articles;
        }

        public ArticleViewModel Get(string id)
        {
            ArticleViewModel articles = new ArticleViewModel();
            var articleCategoryQuery = @"SELECT Articles.*, Categories.*, Comments.*
                                         FROM Articles
                                         INNER JOIN CategoryInArticles ON CategoryInArticles.ArticleId = Articles.Id
                                         INNER JOIN Categories ON Categories.Id = CategoryInArticles.CategoryId
                                         LEFT JOIN Comments  ON Comments.ArticleId = Articles.Id
                                         WHERE Articles.Id = @Id";

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var dictionaryArticle = new Dictionary<Guid, ArticleViewModel>();

                db.Query<ArticleViewModel, Category, CommentViewModel, ArticleViewModel>(articleCategoryQuery, (art, cat, com) =>
                {
                    ArticleViewModel ArticleViewModel = new ArticleViewModel();
                    if (!dictionaryArticle.TryGetValue(art.Id, out ArticleViewModel))
                    {
                        dictionaryArticle.Add(art.Id, ArticleViewModel = art);
                    }

                    if (ArticleViewModel.Category == null)
                    {
                        ArticleViewModel.Category = new List<Category>();
                    }

                    if (ArticleViewModel.Category.Find(x => x.Id == cat.Id) == null)
                    {
                        ArticleViewModel.Category.Add(cat);
                    }

                    if (ArticleViewModel.Comment == null)
                    {
                        ArticleViewModel.Comment = new List<CommentViewModel>();
                    }

                    if (com != null && ArticleViewModel.Comment.Find(x => x.Id == com.Id) == null)
                    {
                        ArticleViewModel.Comment.Add(com);
                    }

                    return ArticleViewModel;
                }, new { Id = id }).AsQueryable();

                articles = dictionaryArticle.Values.FirstOrDefault();
            }
            return articles;
        }

        public void Create(Article post)
        {
            using (SqlConnection db = new SqlConnection(_connectionString))
            {
                var sqlQuery = @"INSERT 
                                  INTO Articles (Id, Title, Description, Author, DateTime) 
                                  VALUES(@Id, @Title, @Description, @Author, @DateTime)";
                 db.Query(sqlQuery, post);
                //Article art = post;
                //db.Insert(art);
            }
        }

        public void Delete(string id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                Article article = new Article { Id = new Guid(id) };
                db.Delete(article);
            }
        }
    }
}
