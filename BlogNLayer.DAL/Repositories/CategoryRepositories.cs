﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogNLayer.Entities;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using Dapper.Contrib.Extensions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace BlogNLayer.Repositories
{
    public class CategoryRepositories
    {
        private string _connectionString;

        public CategoryRepositories(string connectionString)
        {
            _connectionString = connectionString;
        }

        public List<Category> GetAll()
        {
            List<Category> categories;
            
            using (SqlConnection db = new SqlConnection(_connectionString))
            {
                categories = db.GetAll<Category>().ToList();
            }
            return categories;
        }

        public List<Category> GetAllByArticle(string id)
        {
            List<Category> articles;
            var query = @"SELECT * 
                          FROM Categories AS C 
                          LEFT JOIN CategoryInArticles AS CIA
                          ON C.Id = CIA.CategoryId
                          WHERE CIA.ArticleId = @id";

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                articles = db.Query<Category>(query, new { id }).ToList();
            }
            return articles;
        }

        public Category Get(string id)
        {
            Category category = null;
            using (SqlConnection db = new SqlConnection(_connectionString))
            {
                category = db.Get<Category>(id);
            }
            return category;
        }

        public void Create(Category post)
        {
            using (SqlConnection db = new SqlConnection(_connectionString))
            {
                  db.Insert(post);
            }
        }

        public void Delete(string id)
        {
            using (SqlConnection db = new SqlConnection(_connectionString))
            {
                Category category = new Category { Id = new Guid(id) };  
                db.Delete(category);
            }
        }
    }
}
