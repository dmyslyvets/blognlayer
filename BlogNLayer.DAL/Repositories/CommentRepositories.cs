﻿using System.Collections.Generic;
using System.Linq;
using BlogNLayer.Entities;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using Dapper.Contrib.Extensions;

namespace BlogNLayer.Repositories
{
   public class CommentRepositories
    {
        private string _connectionString;

        public CommentRepositories(string connectionString)
        {
            _connectionString = connectionString;
        }

        public List<Comment> GetAllForArticle(string id)
        {
            List<Comment> comments;
            var query = "SELECT * FROM Comments WHERE ArticleId = @id";
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                comments = db.Query<Comment>(query, new { id }).ToList();
            }
            return comments;
        }

        public Comment Get(string id)
        {
            Comment comment  = new Comment();
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                comment = db.Get<Comment>(id);
            }
            return comment;
        }

        public void Create(Comment post)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                db.Insert(post);
            }
        }

        public void DeleteForArticle(string id)
        {
            var query = "DELETE FROM Comments WHERE ArticleId = @id";

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                db.Execute(query, new { id });
            }
        }

        public void Delete(string id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                Comment comment = new Comment { Id = new System.Guid(id) };
                db.Delete(comment);
            }
        }
    }
}
